import org.example.model.CurrencyManager;
import org.example.model.DTO.ReceivedCurrencies;
import org.example.model.DTO.SendCurrencies;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class AppTests {

    private static final String RESPONSE_STUB = " {\n" +
            "  \"reqID\": \"R231312\",\n" +
            "  \"reqDate\" : \"2023-04-28\",\n" +
            "  \"bic\" : \"040173745\",\n" +
            "  \"firstCurrency\": \"EUR\",\n" +
            "  \"secondCurrency\":  \"USD\",\n" +
            "  \"firstCurrencyName\": \"Evro\",\n" +
            "  \"secondCurrencyName\": \"Dollar\",\n" +
            "  \"course\": 15.323\n" +
            "}";
    private static final String SEND_JSON = "{\"bic\":\"040173745\"," +
            "\"curr1\":\"EUR\"," +
            "\"curr2\":\"USD\"}";

    private static ClientAndServer mockServer;

    @BeforeAll
    public static void startMockServer() {
        mockServer = ClientAndServer.startClientAndServer(8081);
        new MockServerClient("localhost",8081)
                .when(
                        request()
                                .withMethod("POST")
                                .withPath("/send")
                                .withHeader("Accept", "application/json")
                                .withHeader("Content-type", "application/json")
                                .withBody(SEND_JSON)
                ).respond(
                        response()
                                .withHeader("Content-type", "application/json")
                                .withBody(RESPONSE_STUB)
                );

    }

    @AfterAll
    public static void stopMockServer() {
        mockServer.stop();
    }

    @Test
    public void getCourseTest() throws IOException {
        CurrencyManager manager = new CurrencyManager();
        assertEquals(manager.getCourse(SEND_JSON, "http://127.0.0.1:8081/send"), new ReceivedCurrencies(
                "R231312", "2023-04-28", "040173745",
                "EUR", "USD", "Evro",
                "Dollar", "15.323"));


    }

    @Test
    public void pojoToJsonTest() {
        CurrencyManager manager = new CurrencyManager();
        SendCurrencies sendCurrencies = new SendCurrencies(
                "040173745", "EUR", "USD");
        assertEquals(manager.pojoToJson(sendCurrencies),  SEND_JSON);

    }

}
