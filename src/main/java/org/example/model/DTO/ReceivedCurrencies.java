package org.example.model.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ReceivedCurrencies {

    private String reqID;
    private String reqDate;
    private String bik;
    private String firstCharCode;
    private String secondCharCode;
    private String firstName;
    private String secondName;
    private String value;
}
