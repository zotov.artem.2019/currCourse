package org.example.model.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class SendCurrencies {
    private String bik;
    private String firstCharCode;
    private String secondCharCode;
}
