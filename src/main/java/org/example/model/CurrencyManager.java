package org.example.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.generated.BicCode;
import org.example.generated.ObjectFactory;
import lombok.extern.java.Log;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.example.model.DTO.ReceivedCurrencies;
import org.example.model.DTO.SendCurrencies;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.util.*;
import java.util.logging.Level;

@Log
public class CurrencyManager {

    private static final String AUSTRALIAN_DOLLAR_CHARCODE = "AUD";
    private static final String URI = "https://cbr.ru/scripts/XML_bic.asp";
    private static final String START_UNMARSHALLING_MSG = "Start unmarshalling";
    private static final String END_UNMARSHALLING_MSG = "End unmarshalling";
    private static final String EXCEPTION = "Exception: ";
    private static final String START_SERIALIZING_MSG = "Start serializing";
    private static final String END_SERIALIZING_MSG = "End serializing";
    private static final String START_DESERIALIZATION_MSG = "Start deserialization";
    private static final String END_DESERIALIZATION_MSG = "End deserialization";
    private static final String ENCODE = "UTF-8";
    private static final String ACCEPT_HEADER = "Accept";
    private static final String CONTENT_TYPE_HEADER = "Content-type";
    private static final String APPLICATION_TYPE = "application/json";
    private static final int SUCCESSFUL_STATUS_CODE = 200;
    private static final int REDIRECTION_STATUS_CODE = 300;
    private static final String DOLLAR_CHARCODE = "USD";
    private static final String EURO_CHARCODE = "EUR";
    private static final String BRITISH_POUND_STERLING_CHARCODE = "GBP";
    private static final String BELARUSSIAN_RUBLE_CHARCODE = "BYN";
    private static final String EGYPTIAN_POUND_CHARCODE = "EGP";
    private static final String CHINA_YUAN_CHARCODE = "CNY";
    private static final String DANISH_KRONE_CHARCODE = "DKK";
    private static final String SINGAPORE_DOLLAR_CHARCODE = "SGD";
    private static final String TURKISH_LIRA_CHARCODE = "TRY";
    private static final String INDIAN_RUPEE_CHARCODE = "INR";
    private static final String JAPANESE_YEN_CHARCODE = "JPY";
    private static final String KAZAKHSTAN_TENGE_CHARCODE = "KZT";
    private static final String SWISS_FRANK_CHARCODE = "CHF";
    private final ObjectFactory factory = new ObjectFactory();

    private final RequestConfig config;

    public CurrencyManager() {

        config = RequestConfig.custom()
                .setConnectTimeout(5000)
                .setConnectionRequestTimeout(5000)
                .setSocketTimeout(5000)
                .build();
    }

    public ReceivedCurrencies getCourse(String json, String uri) throws IOException{
        final HttpPost httpPost = new HttpPost(uri);
        StringEntity entity;
        entity = new StringEntity(json, ENCODE);
        httpPost.setEntity(entity);
        httpPost.setHeader(ACCEPT_HEADER, APPLICATION_TYPE);
        httpPost.setHeader(CONTENT_TYPE_HEADER, APPLICATION_TYPE);
        ReceivedCurrencies res = null;
        try (CloseableHttpClient client = HttpClientBuilder.create()
                .setDefaultRequestConfig(config).build();
             CloseableHttpResponse response = client.execute(httpPost)) {
            log.info("message sent");
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode >= SUCCESSFUL_STATUS_CODE && statusCode < REDIRECTION_STATUS_CODE) {
                res = jsonToPojo(response.getEntity().getContent());
                log.info("message with request ID: " + res.getReqID() + " was received");

            }
        }
        return res;
    }

    public BicCode unmarshal() {
        log.info(START_UNMARSHALLING_MSG);
        BicCode unmarshal = factory.createBicCode();
        try (CloseableHttpClient client = HttpClients.createDefault();
             CloseableHttpResponse response = client.execute(new HttpGet(URI))
        ) {
            JAXBContext context = JAXBContext.newInstance(BicCode.class);
            unmarshal = (BicCode) context.createUnmarshaller()
                    .unmarshal(response.getEntity().getContent());
        } catch (JAXBException | IOException ex) {
            log.log(Level.SEVERE, EXCEPTION, ex);
        }
        log.info(END_UNMARSHALLING_MSG);
        return unmarshal;
    }

    public String pojoToJson(SendCurrencies currencies) {
        log.info(START_SERIALIZING_MSG);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = null;
        try {
            json = objectMapper.writeValueAsString(currencies);
        } catch (IOException ex) {
            log.log(Level.SEVERE, EXCEPTION, ex);
        }
        log.info(END_SERIALIZING_MSG);
        return json;
    }

    private ReceivedCurrencies jsonToPojo(InputStream json) {
        ReceivedCurrencies currencies = null;
        log.info(START_DESERIALIZATION_MSG);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            currencies = objectMapper.readValue(json, ReceivedCurrencies.class);
        } catch (IOException ex) {
            log.log(Level.SEVERE, EXCEPTION, ex);
        }
        log.info(END_DESERIALIZATION_MSG);
        return currencies;

    }

    public String getRandomBic(BicCode bicCode) {
        List<BicCode.Record> records = bicCode.getRecord();
        Random rand = new Random();
        BicCode.Record rec = records.get(rand.nextInt(records.size()));
        return rec.getBic();
    }

    private HashSet<String> getCurrencies() {
        HashSet<String> currencies = new HashSet<>();
        currencies.add(AUSTRALIAN_DOLLAR_CHARCODE);
        currencies.add(BRITISH_POUND_STERLING_CHARCODE);
        currencies.add(BELARUSSIAN_RUBLE_CHARCODE);
        currencies.add(DOLLAR_CHARCODE);
        currencies.add(EGYPTIAN_POUND_CHARCODE);
        currencies.add(CHINA_YUAN_CHARCODE);
        currencies.add(DANISH_KRONE_CHARCODE);
        currencies.add(SINGAPORE_DOLLAR_CHARCODE);
        currencies.add(TURKISH_LIRA_CHARCODE);
        currencies.add(EURO_CHARCODE);
        currencies.add(INDIAN_RUPEE_CHARCODE);
        currencies.add(JAPANESE_YEN_CHARCODE);
        currencies.add(KAZAKHSTAN_TENGE_CHARCODE);
        currencies.add(SWISS_FRANK_CHARCODE);
        return currencies;
    }

    public String getRandomCurrency() {
        HashSet<String> currencies = getCurrencies();
        List<String> copy = new ArrayList<>(currencies);
        Collections.shuffle(copy);
        return copy.get(0);
    }


}
