package org.example.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.example.generated.BicCode;
import lombok.extern.java.Log;
import org.example.model.CurrencyManager;
import org.example.model.DTO.ReceivedCurrencies;
import org.example.model.DTO.SendCurrencies;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;

@Log
public class CurrencyCourseController {

    private static final String START_BUTTON_PRESSED_MSG = "Start button pressed";
    private static final String EXCEPTION = "Exception: ";
    private static final String INITIALIZING_TABLE_MSG = "Initializing table";
    private static final String BIC_PROPERTY = "bik";
    private static final String FIRST_CURRENCY_NAME_PROPERTY = "firstName";
    private static final String SECOND_CURRENCY_NAME_PROPERTY = "secondName";
    private static final String COURSE_PROPERTY = "value";
    private static final String URI = "http://localhost:8081/post";

    @FXML
    private TableView<ReceivedCurrencies> currData;

    @FXML
    private TableColumn<ReceivedCurrencies, String> bic;

    @FXML
    private TableColumn<ReceivedCurrencies, String> cur1;

    @FXML
    private TableColumn<ReceivedCurrencies, String> cur2;

    @FXML
    private TableColumn<ReceivedCurrencies, Double> course;

    @FXML
    private Button startButton;

    @FXML
    private Button stopButton;

    private final ObservableList<ReceivedCurrencies> receivedCurrencies =
            FXCollections.observableArrayList();

    @FXML
    void initialize() {
        CurrencyManager manager = new CurrencyManager();
        AtomicReference<BicCode> bicCode = new AtomicReference<>();
        AtomicReference<String> bik = new AtomicReference<>();
        AtomicReference<ScheduledExecutorService> service = new AtomicReference<>();
        startButton.setOnAction(event -> {
            service.set(Executors.newScheduledThreadPool(10));
            bicCode.set(manager.unmarshal());
            log.info(START_BUTTON_PRESSED_MSG);
            service.get().scheduleAtFixedRate(() -> {
                if (!stopButton.isPressed()) {
                    bik.set(manager.getRandomBic(bicCode.get()));
                    String curr1 = manager.getRandomCurrency();
                    String curr2;
                    do {
                        curr2 = manager.getRandomCurrency();
                    } while (curr1.equals(curr2));
                    String json = convertToJson(manager, bik, curr1, curr2);// отправляемые данные
                    ReceivedCurrencies currencies = null;
                    try {
                        currencies = manager.getCourse(json, URI); //полученные данные
                    } catch (SocketTimeoutException e) {
                        log.log(Level.SEVERE, EXCEPTION, e);
                        service.get().shutdown();
                    }
                    catch (IOException e) {
                        log.log(Level.SEVERE, EXCEPTION, e);
                        throw new RuntimeException(e);
                    }
                    receivedCurrencies.add(currencies);
                    initTable();
                } else {
                    service.get().shutdown();
                }
            }, 0, 50, TimeUnit.MILLISECONDS);

        });
    }
    private static String convertToJson(CurrencyManager manager, AtomicReference<String> bik, String curr1, String curr2) {
        return manager.pojoToJson(
                new SendCurrencies(
                        bik.get(), curr1,
                        curr2));
    }

    private void initTable() {
        log.info(INITIALIZING_TABLE_MSG);
        bic.setCellValueFactory(new PropertyValueFactory<>(BIC_PROPERTY));
        cur1.setCellValueFactory(new PropertyValueFactory<>(FIRST_CURRENCY_NAME_PROPERTY));
        cur2.setCellValueFactory(new PropertyValueFactory<>(SECOND_CURRENCY_NAME_PROPERTY));
        course.setCellValueFactory(new PropertyValueFactory<>(COURSE_PROPERTY));
        currData.setItems(receivedCurrencies);
    }


}
