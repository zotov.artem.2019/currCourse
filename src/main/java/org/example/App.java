package org.example;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.extern.java.Log;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogManager;

@Log
public class App extends Application {

    public static final String RESOURCE_NAME = "/curr.fxml";
    public static final String TITLE = "Currency course";
    public static final String LOG_START_MSG = "Application start";
    public static final String LOG_END_MSG = "Application stop";
    public static final String FXML_LOAD_ERR = "Cant load fxml file";
    public static final String LOG_PROPERTIES_FILE = "/log.properties";

    @Override
    public void start(Stage stage) {
        Scene scene = null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(RESOURCE_NAME));
            scene = new Scene(fxmlLoader.load());
        } catch (IOException e) {
            log.log(Level.SEVERE, FXML_LOAD_ERR);
        }
        stage.setScene(scene);
        stage.setTitle(TITLE);
        stage.show();
    }

    public static void main(String[] args) throws IOException {
        LogManager.getLogManager().readConfiguration(
                App.class.getResourceAsStream(LOG_PROPERTIES_FILE));
        log.info(LOG_START_MSG);
        launch(args);
        log.info(LOG_END_MSG);

    }
}